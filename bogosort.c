#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <stdbool.h>
#include <ctype.h>

bool 
stringisnum (char *s)
{
	for(int i = 0; i < strlen(s); ++i) {
		if(!isdigit(s[i]))
			return false;
	}
	return true;
}

void
printlist(uint8_t *l, int len)
{
	for(unsigned short int i = 0; i <= len; ++i)
		printf("[%d] ", l[i]);
	printf("\n");
}

uint8_t *
bogosort(uint8_t *l, unsigned short int len)
{
	bool sorted = false;
	while(sorted == false) {
		for(unsigned short int i = 0; i <= len; ++i) {
			uint8_t j = rand() % len + 1;
			uint8_t tmp = l[i];
			l[i] = l[j];
			l[j] = tmp;
		}
		sorted = true;
		unsigned short int pos = 0;
		while(pos <= len - 1) {
			if(l[pos] > l[pos + 1]) {
				sorted = false;
			}
			++pos;
		}
	}

	return l;
}

int
main(int argc, char *argv[])
{
	if(argc == 1 || argc > 2 || !stringisnum(argv[1])) {
		printf("Usage: bogosort [length of list]\n");
		return EXIT_FAILURE;
	}

	unsigned int length = atoi(argv[1]) - 1;
	if(length == 0)
		return EXIT_FAILURE;
	
	time_t t;
	srand((unsigned) time(&t));

	uint8_t *list = malloc(length * sizeof(int));
	if(list == NULL) {
		printf("malloc failed.");
		return EXIT_FAILURE;
	}

	for(unsigned int i = 0; i <= length; ++i)
		list[i] = (rand() % 10 + 1);

	printf("unsorted : ");
	printlist(list, length);

	list = bogosort(list, length);

	printf("\nsorted : ");
	printlist(list, length);
	printf("\n");
	free(list);
	fflush(stdout);
	return EXIT_SUCCESS;
}
